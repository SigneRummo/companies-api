package ee.valiit.companiesapi.repository;

 import ee.valiit.companiesapi.model.Company;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.boot.autoconfigure.jms.JmsProperties;
 import org.springframework.jdbc.core.JdbcTemplate;
 import org.springframework.jdbc.core.RowMapper;
 import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CompanyRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Company> fetchCompanies() {
        List<Company> companies = jdbcTemplate.query(
                "SELECT * FROM company", (row, rowNum) -> {
                    return new Company(
                            row.getInt("id"),
                            row.getString("name"),
                            row.getString("logo")
                    );
                });
        return companies;
    }

    public Company fetchCompany(int id) {
        List<Company> companies = jdbcTemplate.query(
                "SELECT * FROM company WHERE id = ?",
                new Object[]{id},
                (row, rowNum) -> {
                    return new Company(
                            row.getInt("id"),
                            row.getString("name"),
                            row.getString("logo")
                    );
                });
        return companies.size() > 0 ? companies.get(0) : null;
    }

public void deleteCompany(int id) {
        jdbcTemplate.update("DELETE FROM company WHERE id = ?", id);
}
public void addCompany(Company company) {
        jdbcTemplate.update("INSERT INTO company (name, logo) VALUES (?,?)",
                company.getName(), company.getLogo());
}
public void updateCompany(Company company) {
        jdbcTemplate.update("UPDATE company SET name = ?, logo = ?, where id = ?)",
                company.getName(), company.getLogo(), company.getId());
}
}
