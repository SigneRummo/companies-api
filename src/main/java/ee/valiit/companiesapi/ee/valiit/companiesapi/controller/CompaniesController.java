package ee.valiit.companiesapi.ee.valiit.companiesapi.controller;

import ee.valiit.companiesapi.model.Company;
import ee.valiit.companiesapi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@CrossOrigin("*")

@RestController("/")
public class CompaniesController {

    @Autowired
    private CompanyRepository companyRepository;

    @GetMapping("/companies")
    public List<Company> fetchCompanies() {
        return companyRepository.fetchCompanies();
    }

    @GetMapping("/company")
    public Company getCompany(@RequestParam("id")int id)
    {
        return companyRepository.fetchCompany(id);
    }

    @DeleteMapping("/company")
    public void deleteCompany(@RequestParam("id")int id)
    {
        companyRepository.deleteCompany(id);
    }
    @PostMapping("/company")
    public void addCompany(
            @RequestBody Company company) {
        companyRepository.addCompany(company);

    }
    @PutMapping("/company")
    public void updateCompany(@RequestBody Company company){
        companyRepository.updateCompany(company);
    }

    @GetMapping
    public String getHello(
            @RequestParam("name") String nameParam,
            @RequestParam("profession") String professionParam) {
        return "Tere, " + nameParam + ", sa oled " + professionParam;
    }

    @PostMapping("/person")
    public void savePerson(@RequestBody Person x) {
        System.out.println("Saime sellise sisendi: " + x.getName() + x.getProfession());

    }

    @GetMapping("/employees")
    public List<Person> getEmployees() {
        return Arrays.asList(
                new Person("Marek", "arendaja"),
                new Person("Teet", "müügijuht"),
                new Person("Malle", "tegevjuht")
        );

    }


    public static class Person {
        private String name;
        private String profession;

        public String getName() {
            return name;
        }

        public String getProfession() {
            return profession;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setProfession(String profession) {
            this.profession = profession;
        }

        public Person(String name, String profession) {
            this.name = name;
            this.profession = profession;
        }

        public Person() {
        }
    }
}
